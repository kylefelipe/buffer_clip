# -*- coding: utf-8 -*-
"""
/***************************************************************************
 BufferClip
                                 A QGIS plugin
 This plugin buffers a line or lines and uses the buffer to clip a raster
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                             -------------------
        begin                : 2019-01-30
        copyright            : (C) 2019 by Kyle Felipe
        email                : kylefelipe@gmail.com
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load BufferClip class from file BufferClip.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .bufferclip import BufferClip
    return BufferClip(iface)
